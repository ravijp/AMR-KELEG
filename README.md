### Hi there 👋

<!--
**AMR-KELEG/AMR-KELEG** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->

<!--START_SECTION:waka-->
![Profile Views](http://img.shields.io/badge/Profile%20Views-6-blue)

![Lines of code](https://img.shields.io/badge/From%20Hello%20World%20I%27ve%20Written-2.6%20million%20lines%20of%20code-blue)

**I'm a Night 🦉** 

```text
🌞 Morning    17 commits     ███░░░░░░░░░░░░░░░░░░░░░░   12.14% 
🌆 Daytime    52 commits     █████████░░░░░░░░░░░░░░░░   37.14% 
🌃 Evening    46 commits     ████████░░░░░░░░░░░░░░░░░   32.86% 
🌙 Night      25 commits     ████░░░░░░░░░░░░░░░░░░░░░   17.86%

```
📅 **I'm Most Productive on Thursday** 

```text
Monday       5 commits      █░░░░░░░░░░░░░░░░░░░░░░░░   3.57% 
Tuesday      16 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   11.43% 
Wednesday    16 commits     ██░░░░░░░░░░░░░░░░░░░░░░░   11.43% 
Thursday     37 commits     ██████░░░░░░░░░░░░░░░░░░░   26.43% 
Friday       20 commits     ███░░░░░░░░░░░░░░░░░░░░░░   14.29% 
Saturday     28 commits     █████░░░░░░░░░░░░░░░░░░░░   20.0% 
Sunday       18 commits     ███░░░░░░░░░░░░░░░░░░░░░░   12.86%

```


📊 **This Week I Spent My Time On** 

```text
⌚︎ Time Zone: Africa/Cairo

```

**I Mostly Code in Python** 

```text
Python                   11 repos            ████████░░░░░░░░░░░░░░░░░   34.38% 
C++                      7 repos             █████░░░░░░░░░░░░░░░░░░░░   21.88% 
HTML                     3 repos             ██░░░░░░░░░░░░░░░░░░░░░░░   9.38% 
Jupyter Notebook         3 repos             ██░░░░░░░░░░░░░░░░░░░░░░░   9.38% 
Java                     2 repos             █░░░░░░░░░░░░░░░░░░░░░░░░   6.25%

```



<!--END_SECTION:waka-->
